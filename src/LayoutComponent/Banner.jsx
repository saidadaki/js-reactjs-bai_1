import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div>
        <div className="py-5">
          <div className="container px-lg-5">
            <div className="p-5 px-lg-5 bg-light text-center">
              <h1 className="pt-5 font-weight-bold">A Warm Welcome</h1>
              <h4 className="py-3">
                Bootstrap utility classes are used to create this jumbotron
                since the old component has been removed from the framework. Why
                create custom CSS when you can use utilities?
              </h4>
              <button className="btn btn-info font-weight-bold">
                Call to action
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
