import logo from './logo.svg';
import './App.css';
import Header from './LayoutComponent/Header';
import Banner from './LayoutComponent/Banner';
import Content from './LayoutComponent/Content';


function App() {
  return (
    <div className="App">
     <Header />
     <Banner />
     <Content />
    </div>
  );
}

export default App;
